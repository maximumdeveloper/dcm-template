// Редактирование информации в личном профиле

$(document).ready(function () {

    // editForm('edit-user-name');
    editForm('edit-user-surname');
    // editForm('edit-user-phone');
    // editForm('edit-user-email');

    function editForm(editBtn) {
        $(document).on('click', '#' + editBtn + '', function () {
            let formName = $('#form-user-name'),
                editName = $('#edit-user-name'),
                textName = $('#user-name');

            if (editBtn === 'edit-user-surname' && $(formName).is(':hidden')) {
                $(editName).text('Сохранить');
                $(textName).hide();
                $(formName).show().focus();

            } else {
                let statusFormName = $(formName).val();

                $(textName).text(statusFormName);

                if (statusFormName !== '') {
                    $(editName).text('Изменить');
                    $(textName).show();
                    $(formName).hide();

                } else {
                    $(formName).addClass('form-invalid');
                }
            }
        });
    }
});









    // $('#edit-name').on('click', function () {
    //     let formControl = $('#form-name');
    //
    //     if ($(formControl).is(':hidden')) {
    //         $('#name').hide();
    //         $('#form-name').show().focus();
    //         $('#save-name').show();
    //         $('#edit-name').hide();
    //     }
    // });
    //
    // $('#save-name').on('click', function () {
    //     let formControl = $('#form-name'),
    //         formControlContent = $(formControl).val();
    //
    //     if (formControlContent !== '') {
    //         $(formControl).removeClass('form-invalid').addClass('form-control');
    //         $('#name').show();
    //         $('#form-name').hide();
    //         $('#save-name').hide();
    //         $('#edit-name').show();
    //
    //     } else {
    //         $(formControl).addClass('form-invalid').focus();
    //     }
    // });