let gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    cssnano         = require('gulp-cssnano'),
    sourcemaps      = require('gulp-sourcemaps'),
    browserSync     = require('browser-sync'),
    autoprefixer    = require('gulp-autoprefixer'),
    concat          = require('gulp-concat'),
    minifyCSS       = require('gulp-minify-css'),
    uglify          = require('gulp-uglifyjs');

    // js

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});


gulp.task('grid', function() {
    return gulp.src('app/libs/sass-grid/grid.scss')
    .pipe(sass().on('grid error', sass.logError))
    .pipe(cssnano())
    .pipe(gulp.dest('app/'));
});

gulp.task('css-libs', function () {
    return gulp.src([
        'app/libs/arcticModal/arcticmodal/jquery.arcticmodal.css',
        'app/libs/slick-carousel/slick/slick.css',
        'app/libs/formstone/dist/css/dropdown.css',
        'app/libs/formstone/dist/css/scrollbar.css',
        'app/libs/formstone/dist/css/navigation.css',
        'app/libs/formstone/dist/css/sticky.css',
        'app/libs/formstone/dist/css/sticky.css',
        'app/libs/formstone/dist/css/tabs.css'
    ])
    .pipe(concat('libs.min.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('app/css'));
});

gulp.task('scripts', function() {
    return gulp.src([
        'app/libs/jquery/dist/jquery.min.js',
        'app/libs/parallax-js/dist/parallax.min.js'
    ])
    .pipe(concat('libs.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/js'));
});

gulp.task('sass', function() {
    return gulp.src('app/sass/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer(['Last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['browser-sync'], function() {
    gulp.watch('app/**/*.html', browserSync.reload);
    gulp.watch('app/js/*.js', browserSync.reload);
    gulp.watch('app/sass/**/*.scss', ['sass']);
});